# My "demo" for DOT demo evening 2023-09-16

This was my attempt at making something for a [demo evening hosted by Digital Media Club of Aalto University DOT](https://dot-ry.fi/news/demo-evening-16.9.2023/). It's written in uxntal, the assembly language for the [uxn virtual machine](https://100r.co/site/uxn.html). The idea was to produce something that runs on the Nintendo DS, which is possible with uxn with the [uxnds](https://github.com/asiekierka/uxnds) port of the VM.

In the end, I didn't have time to make graphics, I only managed to make some sound (that didn't even run perfectly on the DS), with the barely-functional ["sequencer"](./audio-test-1.tal#L117) implemented during the last half hour.

Thinking about it a bit more, it seems that sound system of uxn is geared more towards sound effects for games and such, rather than sequencing a song. Or maybe it's just that I have no idea what I'm doing. But overall I had quite a bit of fun working with the stack-based assembly of uxn, and maybe with a bit more practice it's probably possible to make some proper music with it. I'll definitely try making a demo with it again in the future.


The `shellconfig.xsh` file is a small script to set up my shell ([xonsh](https://xon.sh/)) with some handy commands to quickly assemble and run a rom. It assumes that the `uxnemu` and `uxnasm` programs are located in the folder `../uxn-essentials` relative to this folder.


The code is licensed under the [WTFPL](http://www.wtfpl.net/). The `<print-dec>` routine for printing numbers is copied from the [uxn datetime example](https://git.sr.ht/~rabbits/uxn/tree/main/item/projects/examples/devices/datetime.tal) and the sine-wave PCM data is copied from the [piano example](https://git.sr.ht/~rabbits/uxn/tree/3293a1b6399d9160f4c4ad7af96e6d1990cd7229/item/projects/examples/demos/piano.tal#L460), which are licensed under the MIT license.
