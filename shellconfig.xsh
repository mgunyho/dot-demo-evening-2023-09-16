# run this file with 'source shellconfig.xsh' to set up the shell for uxn development

from pathlib import Path

$PATH.append("../uxn-essentials")

def _mkrun(args):
	uxn_cmd = "uxnemu"
	try:
		args.remove("--cli")
		uxn_cmd = "uxncli"
	except ValueError:
		pass

	rom_name = str(Path(args[0]).with_suffix(".rom"))
	res = ![uxnasm @(args[0]) @(rom_name)]
	if res:
		@([uxn_cmd, rom_name])

def _autorun(args):
	uxn_cmd = "uxnemu"
	try:
		args.remove("--cli")
		uxn_cmd = "uxncli"
	except ValueError:
		pass

	fname = args[0]
	rom_name = str(Path(fname).with_suffix(".rom"))
	# don't know how to make this work with the _mkrun function above...
	# (can't # figure out how to call xonsh function from entr) so just
	# hardcoding / copy-pasting uxnasm && uxnemu here.
	echo @(fname) | @(["entr", "-cr", "sh", "-c", f"uxnasm {fname} {rom_name} && {uxn_cmd} {rom_name}"])



aliases["mkrun"] = _mkrun
aliases["autorun"] = _autorun
